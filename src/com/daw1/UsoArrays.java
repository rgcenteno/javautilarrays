/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daw1;

import java.util.Arrays;
/**
 *
 * @author rgcenteno
 */
public class UsoArrays {
    static java.util.Scanner teclado = new java.util.Scanner(System.in);
    
    final private static int test = 1;
    final private static int[][] matriz = {
        {16, 3, 2, 13},

        {5, 10, 11, 8},

        {9, 6, 7, 12},

       {4, 15, 14, 1}
    };
    
    private static String [] nombre = {"Carlos", "Miguel", "Iván", "Benjamín", "Francisco", "Erik", "Alexis Jose", "Marcos", "Cristopher", "Mauricio", "Jose Simon", "Nuria Maria"};
    final private static String [] apellido1 = {"Alvarez", "Candeira", "Casas", "Dominguez", "Fernandez", "Ferreira", "Giraldez", "Gonzalez", "Juncal", "Montes", "Sanchez", "da Silva", "Suarez"};
    final private static String [] apellido2 = {"Sanchez", "Carrera", "Cerqueira", "Fernandez", "Araujo", "Oset", "Groba", "Pereira", "Abeledo", "Iglesias", "Gonzalez", "Vilas"};


    final private static int[] numeros = { 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010 };

    final private static int[] numeros2 = { 2001, 2002, 2003, 2004, 2005, 2016, 2007, 2008, 2009, 2010 };
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {        
        String input = "";
        do{
            System.out.println("************************************");
            System.out.println("*  1. Crear array con valores      *");
            System.out.println("*  2. Ordenar arrays               *");
            System.out.println("*  3. Buscar apellido              *");
            System.out.println("*  4. Primera posición diferente   *");            
            System.out.println("************************************");
            input = teclado.nextLine();
            String cadena;
            switch(input){                
                case "1":                    
                    crearArrayConValores();
                    break;
                case "2":
                    ejercicio2();
                    break;
                case "3":
                    ejercicio3();
                    break;
                case "4":
                    primeraPosicionDiferente();
                    break;
            }
        }while(!input.equals("0"));
    }        
    
    private static void crearArrayConValores(){        
        
        int tamano = 0;        
        do{
            System.out.println("Inserte el tamaño del array (>0)");
            if(teclado.hasNextInt()){
                tamano = teclado.nextInt();
                if(tamano <= 0){
                    System.out.println("El tamaño debe ser mayor que cero");
                }
            }
            teclado.nextLine();
        }while(tamano <= 0);
        
        int valor = -1;
        boolean correcto = false;
        do{
            System.out.println("Inserte el valor inicial");
            if(teclado.hasNextInt()){
                valor = teclado.nextInt();
                correcto = true;
            }
            teclado.nextLine();
        }while(!correcto);
        int[] array = new int[tamano];
        java.util.Arrays.fill(array, valor);
        System.out.println(java.util.Arrays.toString(array));
    }
    
    private static void ejercicio2(){
        Arrays.sort(nombre);
        Arrays.sort(apellido1);
        Arrays.sort(apellido2);
        System.out.println(Arrays.toString(nombre));
        System.out.println(Arrays.toString(apellido1));
        System.out.println(Arrays.toString(apellido2));
    }
    
    private static void ejercicio3(){        
        System.out.println("Inserte el apellido a buscar");
        String apellido = teclado.nextLine();
        System.out.println("El apellido " + apellido + " está en el array? " + estaEnArrays(apellido));
        
    }
    
    private static boolean estaEnArrays(String busqueda){
        Arrays.sort(apellido1);
        Arrays.sort(apellido2);
        int res1 = Arrays.binarySearch(apellido1, busqueda);
        int res2 = Arrays.binarySearch(apellido2, busqueda);
        return res1 >= 0 || res2 >= 0;
    }
    
    private static void primeraPosicionDiferente(){
        System.out.println("La primera posición diferente es: " + Arrays.mismatch(numeros, numeros2));
    }
    
    /*************************************************************************************************************************/
    
    private static void testsReferencia(){
        System.out.println(test);//1
        incrementarInt(test);
        System.out.println(test);//1        
        
        System.out.println(java.util.Arrays.toString(numeros));
        System.out.println(numeros);
        incrementarArray(numeros);
        System.out.println(java.util.Arrays.toString(numeros));        
        
        System.out.println(java.util.Arrays.toString(matriz));
        System.out.println(java.util.Arrays.deepToString(matriz));
        System.out.println(java.util.Arrays.deepToString(nombre));
    }
    
    private static void incrementarArray(int[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] += 1000;            
        }
    }
    
    private static void incrementarInt(int t){
        t += 1000;
    }
    
}
